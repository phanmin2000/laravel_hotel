<!DOCTYPE html>
<html lang="en">
<head>
    @include('client.share.css')
    @stack('css')
</head>
<body>
@include('client.share.header')
    @yield('content')
<!-- homepage content end -->
@include('client.share.footer')
@include('client.share.js')
@yield('js')
</body>
</html>
