@extends('client.share.master')
@push('css')
    <link rel="stylesheet" href="{{ asset('client/css/room.css') }}" />
    <style>
        table {
            border-collapse: collapse;
            width: 500px !important;
        }

        th,
        td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #f2f2f2;
        }

        tr:hover {
            background-color: #f5f5f5;
        }
    </style>
@endpush

<body>
    @section('content')
        <header class="page">
            <div class="container">
                <ul class="breadcrumbs d-flex flex-wrap align-content-center">
                    <li class="list-item">
                        <a class="link" href="{{ route('home') }}">Home</a>
                    </li>

                    <li class="list-item">
                        <a class="link" href="{{ route('list-room') }}">Rooms</a>
                    </li>
                    <li class="list-item">
                        <a class="link" href="#">{{ $data->ma_phong }}</a>
                    </li>
                </ul>
                <h1 class="page_title">Loại phòng {{ $data->ma_phong }}</h1>
            </div>
        </header>
        <!-- single room content start -->
        <main>
            <!-- room section start -->
            <div class="room section">
                <div class="container">
                    <div class="room_main d-lg-flex flex-wrap align-items-start">
                        <div class="room_main-slider col-12 d-lg-flex">
                            <div class="room_main-slider_view col-lg-8">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <picture>
                                            <source data-srcset="{{ asset($data->hinh_anh) }}"
                                                srcset="{{ asset($data->hinh_anh) }}" />
                                            <img class="lazy" data-src="{{ asset($data->hinh_anh) }}"
                                                src="{{ asset($data->hinh_anh) }}" alt="media" />
                                        </picture>
                                    </div>
                                    <div class="swiper-slide">
                                        <picture>
                                            <source data-srcset="{{ asset($data->hinh_anh) }}"
                                                srcset="{{ asset($data->hinh_anh) }}" />
                                            <img class="lazy" data-src="{{ asset($data->hinh_anh) }}"
                                                src="{{ asset($data->hinh_anh) }}" alt="media" />
                                        </picture>
                                    </div>
                                    <div class="swiper-slide">
                                        <picture>
                                            <source data-srcset="{{ asset($data->hinh_anh) }}"
                                                srcset="{{ asset($data->hinh_anh) }}" />
                                            <img class="lazy" data-src="{{ asset($data->hinh_anh) }}"
                                                src="{{ asset($data->hinh_anh) }}" alt="media" />
                                        </picture>
                                    </div>
                                </div>
                                <div class="swiper-controls d-flex align-items-center justify-content-between">
                                    <a class="swiper-button-prev d-inline-flex align-items-center justify-content-center"
                                        href="#">
                                        <i class="icon-arrow_left icon"></i>
                                    </a>
                                    <a class="swiper-button-next d-inline-flex align-items-center justify-content-center"
                                        href="#">
                                        <i class="icon-arrow_right icon"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="room_main-slider_thumbs">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <picture>
                                            <source data-srcset="{{ asset($data->hinh_anh) }}"
                                                srcset="{{ asset($data->hinh_anh) }}" />
                                            <img class="lazy" data-src="{{ asset($data->hinh_anh) }}"
                                                src="{{ asset($data->hinh_anh) }}" alt="media" />
                                        </picture>
                                    </div>
                                    <div class="swiper-slide">
                                        <picture>
                                            <source data-srcset="{{ asset($data->hinh_anh) }}"
                                                srcset="{{ asset($data->hinh_anh) }}" />
                                            <img class="lazy" data-src="{{ asset($data->hinh_anh) }}"
                                                src="{{ asset($data->hinh_anh) }}" alt="media" />
                                        </picture>
                                    </div>
                                    <div class="swiper-slide">
                                        <picture>
                                            <source data-srcset="{{ asset($data->hinh_anh) }}"
                                                srcset="{{ asset($data->hinh_anh) }}" />
                                            <img class="lazy" data-src="{{ asset($data->hinh_anh) }}"
                                                src="{{ asset($data->hinh_anh) }}" alt="media" />
                                        </picture>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="room_main-info col-lg-8">
                            <div class="amenities d-flex flex-wrap align-items-center">
                                <span class="amenities_item d-inline-flex align-items-center">
                                    <i class="icon-user icon"></i>
                                    {{ $data->so_khach }} người / phòng
                                </span>
                                <span class="amenities_item d-inline-flex align-items-center">
                                    <i class="icon-bunk_bed icon"></i>
                                    {{ $data->view }}
                                </span>

                            </div>
                            <div class="description">
                                <p class="description_text">
                                    {{ $data->mo_ta_phong }}
                                </p>

                            </div>
                            <section class="facilities">
                                <h4 class="facilities_header">Danh sách phòng</h4>
                                <div class="facilities_list d-sm-flex flex-wrap">
                                    @foreach ($chitietphong as $key => $value)
                                        @if ($value->is_open == 0)
                                            <div class="facilities_list-block">
                                                <span class="facilities_list-block_item d-flex align-items-center"
                                                    style="color:#ccc">
                                                    <span class="icon">
                                                        <svg color="#ccc" width="32" height="32"
                                                            viewBox="0 0 32 32" fill="none"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M27.9997 14.7341V7.33329C27.9997 7.15648 27.9294 6.98691 27.8044 6.86189C27.6794 6.73686 27.5098 6.66663 27.333 6.66663H4.66634C4.48953 6.66663 4.31996 6.73686 4.19494 6.86189C4.06991 6.98691 3.99967 7.15648 3.99967 7.33329V14.7341C3.24737 14.8887 2.57136 15.2979 2.0856 15.8928C1.59985 16.4877 1.33405 17.2319 1.33301 18V26C1.33301 26.1768 1.40325 26.3463 1.52827 26.4714C1.65329 26.5964 1.82286 26.6666 1.99967 26.6666H4.66634C4.84315 26.6666 5.01272 26.5964 5.13775 26.4714C5.26277 26.3463 5.33301 26.1768 5.33301 26V22.6666H26.6663V26C26.6663 26.1768 26.7366 26.3463 26.8616 26.4714C26.9866 26.5964 27.1562 26.6666 27.333 26.6666H29.9997C30.1765 26.6666 30.3461 26.5964 30.4711 26.4714C30.5961 26.3463 30.6663 26.1768 30.6663 26V18C30.6654 17.2319 30.3996 16.4877 29.9138 15.8928C29.4281 15.2979 28.752 14.8886 27.9997 14.7341ZM5.33301 7.99996H26.6663V14.6666H23.9997V11.3333C23.9997 11.1565 23.9294 10.9869 23.8044 10.8619C23.6794 10.7369 23.5098 10.6666 23.333 10.6666H17.9997C17.8229 10.6666 17.6533 10.7369 17.5283 10.8619C17.4032 10.9869 17.333 11.1565 17.333 11.3333V14.6666H14.6663V11.3333C14.6663 11.1565 14.5961 10.9869 14.4711 10.8619C14.3461 10.7369 14.1765 10.6666 13.9997 10.6666H8.66634C8.48953 10.6666 8.31996 10.7369 8.19494 10.8619C8.06991 10.9869 7.99967 11.1565 7.99967 11.3333V14.6666H5.33301V7.99996ZM22.6663 14.6666H18.6663V12H22.6663V14.6666ZM13.333 14.6666H9.33301V12H13.333V14.6666ZM29.333 25.3333H27.9997V22C27.9997 21.8231 27.9294 21.6536 27.8044 21.5286C27.6794 21.4035 27.5098 21.3333 27.333 21.3333H4.66634C4.48953 21.3333 4.31996 21.4035 4.19494 21.5286C4.06991 21.6536 3.99967 21.8231 3.99967 22V25.3333H2.66634V18C2.66692 17.4697 2.87782 16.9613 3.25277 16.5864C3.62772 16.2114 4.13609 16.0005 4.66634 16H27.333C27.8633 16.0005 28.3716 16.2114 28.7466 16.5864C29.1215 16.9613 29.3324 17.4697 29.333 18V25.3333Z"
                                                                fill="currentColor" />
                                                        </svg>
                                                    </span>
                                                    Phòng: {{ $value->ten_phong }}
                                                </span>
                                            </div>
                                        @else
                                            <div class="facilities_list-block">
                                                <span class="facilities_list-block_item d-flex align-items-center">
                                                    <span class="icon">
                                                        <svg width="32" height="32" viewBox="0 0 32 32"
                                                            fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M27.9997 14.7341V7.33329C27.9997 7.15648 27.9294 6.98691 27.8044 6.86189C27.6794 6.73686 27.5098 6.66663 27.333 6.66663H4.66634C4.48953 6.66663 4.31996 6.73686 4.19494 6.86189C4.06991 6.98691 3.99967 7.15648 3.99967 7.33329V14.7341C3.24737 14.8887 2.57136 15.2979 2.0856 15.8928C1.59985 16.4877 1.33405 17.2319 1.33301 18V26C1.33301 26.1768 1.40325 26.3463 1.52827 26.4714C1.65329 26.5964 1.82286 26.6666 1.99967 26.6666H4.66634C4.84315 26.6666 5.01272 26.5964 5.13775 26.4714C5.26277 26.3463 5.33301 26.1768 5.33301 26V22.6666H26.6663V26C26.6663 26.1768 26.7366 26.3463 26.8616 26.4714C26.9866 26.5964 27.1562 26.6666 27.333 26.6666H29.9997C30.1765 26.6666 30.3461 26.5964 30.4711 26.4714C30.5961 26.3463 30.6663 26.1768 30.6663 26V18C30.6654 17.2319 30.3996 16.4877 29.9138 15.8928C29.4281 15.2979 28.752 14.8886 27.9997 14.7341ZM5.33301 7.99996H26.6663V14.6666H23.9997V11.3333C23.9997 11.1565 23.9294 10.9869 23.8044 10.8619C23.6794 10.7369 23.5098 10.6666 23.333 10.6666H17.9997C17.8229 10.6666 17.6533 10.7369 17.5283 10.8619C17.4032 10.9869 17.333 11.1565 17.333 11.3333V14.6666H14.6663V11.3333C14.6663 11.1565 14.5961 10.9869 14.4711 10.8619C14.3461 10.7369 14.1765 10.6666 13.9997 10.6666H8.66634C8.48953 10.6666 8.31996 10.7369 8.19494 10.8619C8.06991 10.9869 7.99967 11.1565 7.99967 11.3333V14.6666H5.33301V7.99996ZM22.6663 14.6666H18.6663V12H22.6663V14.6666ZM13.333 14.6666H9.33301V12H13.333V14.6666ZM29.333 25.3333H27.9997V22C27.9997 21.8231 27.9294 21.6536 27.8044 21.5286C27.6794 21.4035 27.5098 21.3333 27.333 21.3333H4.66634C4.48953 21.3333 4.31996 21.4035 4.19494 21.5286C4.06991 21.6536 3.99967 21.8231 3.99967 22V25.3333H2.66634V18C2.66692 17.4697 2.87782 16.9613 3.25277 16.5864C3.62772 16.2114 4.13609 16.0005 4.66634 16H27.333C27.8633 16.0005 28.3716 16.2114 28.7466 16.5864C29.1215 16.9613 29.3324 17.4697 29.333 18V25.3333Z"
                                                                fill="currentColor" />
                                                        </svg>
                                                    </span>
                                                    Phòng: {{ $value->ten_phong }}
                                                </span>
                                            </div>
                                        @endif
                                    @endforeach

                                </div>
                            </section>
                            <section class="rules">
                                <h4 class="rules_header">Đồ dùng trong phòng</h4>
                                <div class="rules_list d-md-flex flex-lg-wrap w-100">
                                    @foreach ($chitietphong as $key => $value)
                                        @if ($key == 0)
                                            {!! $value->noi_that !!}
                                        @endif
                                    @endforeach
                                </div>
                            </section>
                            <section class="rules">
                                <h4 class="rules_header">Luật khách sạn</h4>
                                <div class="rules_list d-md-flex flex-lg-wrap">
                                    <div class="rules_list-block">
                                        <p class="rules_list-block_item d-flex align-items-baseline">
                                            <i class="icon-check icon"></i>
                                            Thời gian nhận phòng 14-00. Thời gian trả phòng 12-00
                                        </p>
                                        <p class="rules_list-block_item d-flex align-items-baseline">
                                            <i class="icon-check icon"></i>
                                            Quy trình thanh toán chỉ diễn ra sau khi thanh toán đầy đủ
                                        </p>
                                        <p class="rules_list-block_item d-flex align-items-baseline">
                                            <i class="icon-check icon"></i>
                                            Quy trình thanh toán tại ký túc xá chỉ diễn ra sau khi có mặt hộ chiếu
                                        </p>
                                    </div>

                                </div>
                            </section>
                            <div class="rating">
                                <span class="rating_summary">
                                    <span class="h2">4.25</span>
                                    <sup class="h4">/5</sup>
                                </span>
                                <div class="rating_list d-flex flex-wrap">
                                    <div class="rating_list-item d-sm-flex align-items-center justify-content-between"
                                        data-order="1">
                                        <span class="label">Location</span>
                                        <span class="progressLine" id="location" data-value="4.7"
                                            data-fill="#0DA574"></span>
                                    </div>
                                    <div class="rating_list-item d-sm-flex align-items-center justify-content-between"
                                        data-order="2">
                                        <span class="label">Comfort</span>
                                        <span class="progressLine" id="comfort" data-value="4.5"
                                            data-fill="#0DA574"></span>
                                    </div>
                                    <div class="rating_list-item d-sm-flex align-items-center justify-content-between"
                                        data-order="3">
                                        <span class="label">Pricing</span>
                                        <span class="progressLine" id="pricing" data-value="4.9"
                                            data-fill="#0DA574"></span>
                                    </div>
                                    <div class="rating_list-item d-sm-flex align-items-center justify-content-between"
                                        data-order="4">
                                        <span class="label">Service</span>
                                        <span class="progressLine" id="service" data-value="4.8"
                                            data-fill="#0DA574"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="room_main-cards col-lg-4">
                            <div class="room_main-cards_card">
                                <span class="pricing">
                                    <span
                                        class="pricing_price h3">{{ number_format($data->gia_mac_dinh, 0, ',', '.') }}đ</span>
                                    / 1 đêm
                                </span>
                                <form class="booking" action="{{ route('booking-process') }}" method="post"
                                    autocomplete="off" data-type="booking">
                                    @csrf
                                    <input type="hidden" name="room_id" value="{{ $data->id }}" />
                                    <div class="booking_group d-flex flex-column">
                                        <label class="booking_group-label h5" for="checkIn">Ngày đặt</label>
                                        <div class="booking_group-wrapper">
                                            <i class="icon-calendar icon"></i>
                                            <input class="booking_group-field field required" data-type="date"
                                                data-start="true" type="text" id="checkIn" name="start_date"
                                                placeholder="Chọn ngày đến" readonly />
                                            <i class="icon-chevron_down icon"></i>
                                        </div>
                                    </div>
                                    <div class="booking_group d-flex flex-column">
                                        <label class="booking_group-label h5" for="checkOut">Ngày trả phòng</label>
                                        <div class="booking_group-wrapper">
                                            <i class="icon-calendar icon"></i>
                                            <input class="booking_group-field field required" data-type="date"
                                                data-end="true" type="text" id="checkOut" name="end_date"
                                                placeholder="Chọn ngày trả phòng" readonly />
                                            <i class="icon-chevron_down icon"></i>
                                        </div>
                                    </div>
                                    <div class="booking_group d-flex flex-column">
                                        <label class="booking_group-label h5" for="checkOut">Tên người đặt</label>
                                        <div class="booking_group-wrapper" style="align-items:center">
                                            <i class="icon-user icon" style="top:unset"></i>
                                            <input class="booking_group-field field required" type="text"
                                                id="ten_nguoi_dat" name="name" placeholder="Nhập tên người đặt" value="{{ $user == null ? null : $user->full_name }}">
                                        </div>
                                    </div>
                                    <div class="booking_group d-flex flex-column">
                                        <label class="booking_group-label h5" for="checkOut">Email xác nhận</label>
                                        <div class="booking_group-wrapper" style="align-items:center">
                                            <i class="icon-user icon" style="top:unset"></i>
                                            <input class="booking_group-field field required" type="text"
                                                id="email_nguoi_dat" name="email" placeholder="Nhập email xác nhận" value="{{ $user == null ? null : $user->email }}">
                                        </div>
                                    </div>
                                    <div class="booking_group d-flex flex-column">
                                        <label class="booking_group-label h5" for="checkOut">Số điện thoại</label>
                                        <div class="booking_group-wrapper" style="align-items:center">
                                            <i class="icon-phone icon" style="top:unset"></i>
                                            <input class="booking_group-field field required" type="number"
                                                id="phone_number" name="phone" placeholder="Nhập sdt liên hệ" value="{{ $user == null ? null : $user->phone }}">
                                        </div>
                                    </div>
                                    <div class="booking_group d-flex flex-column">
                                        <label class="booking_group-label h5" for="checkOut">Số Luợng</label>
                                        <div class="booking_group-wrapper" style="align-items:center">
                                            <i class="icon-phone icon" style="top:unset"></i>
                                            <input class="booking_group-field field required" type="number"
                                                id="so_luong" name="quantity" placeholder="Số lượng phòng">
                                        </div>
                                    </div>

                                    <button class="booking_btn btn theme-element theme-element--accent js-submit-order"
                                        type="submit">Đặt
                                        phòng
                                    </button>
                                </form>
                            </div>
                            <div class="room_main-cards_card accent">

                                <h3 class="title">Ở càng nhiều, giá càng rẻ</h3>
                                <p class="text">Áp dụng cho tất cả khách hàng </p>
                                <div class="content">
                                    <p class="text">Tích kiệm <b>30%</b> khi thuê trên 10 ngày</p>
                                    <p class="text">Tích kiệm <b>20%</b> khi thuê từ 7 ngày trở lên
                                    </p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- room section start -->
            <!-- recommendation section start -->
            <!-- recommendation section end -->
            <!-- stages section start -->
            <!-- stages section end -->
            <!-- rooms section start -->
            <section class="rooms section--blockbg section">
                <div class="block"></div>
                <div class="container">
                    <div class="rooms_header d-sm-flex justify-content-between align-items-center">
                        <h2 class="rooms_header-title" data-aos="fade-right">Các loại phòng khác</h2>
                        <div class="wrapper" data-aos="fade-left">
                            <a class="btn theme-element theme-element--light" href="rooms.html">View all rooms</a>
                        </div>
                    </div>
                    <ul class="rooms_list d-md-flex flex-wrap">
                        @foreach ($list as $key => $value)
                            @if ($key == 2)
                                <li class="rooms_list-item col-md-6 col-xl-4" data-order="{{ $key + 1 }}"
                                    data-aos-delay="50" data-aos="fade-up">
                                    <div class="item-wrapper d-md-flex flex-column">
                                        <div class="media">
                                            <picture>
                                                <source data-srcset="{{ asset($value->hinh_anh) }}"
                                                    srcset="{{ asset($value->hinh_anh) }}" />
                                                <img class="lazy" data-src="images/hotels.jpg" src="images/hotels.jpg"
                                                    alt="media" />
                                            </picture>
                                            <span class="media_label media_label--pricing">
                                                <span
                                                    class="price h4">{{ number_format($value->gia_mac_dinh, 0, ',', '.') }}đ</span>
                                                / 1 đêm
                                            </span>
                                        </div>
                                        <div class="main d-md-flex flex-column justify-content-between flex-grow-1">
                                            <a class="main_title h4" href="room.html" data-shave="true">Loại phòng:
                                                <strong>{{ $value->ma_phong }}</strong> </a>
                                            <div class="main_amenities">
                                                <span class="main_amenities-item d-inline-flex align-items-center">
                                                    <i class="icon-user icon"></i>
                                                    Số chỗ: <strong style="margin-left:4px">{{ $value->so_khach }}
                                                        người</strong>
                                                </span>
                                                <br>
                                                <span class="main_amenities-item d-inline-flex align-items-center">
                                                    <i class="icon-bunk_bed icon"></i>
                                                    View: <strong style="margin-left:4px">{{ $value->view }}</strong>
                                                </span>
                                            </div>
                                            <a class="link link--arrow d-inline-flex align-items-center"
                                                href="{{ route('detail-room', $value->id) }}">
                                                Xem chi tiết
                                                <i class="icon-arrow_right icon"></i>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            @else
                                <li class="rooms_list-item col-md-6 col-xl-4" data-order="{{ $key + 1 }}"
                                    data-aos="fade-up">
                                    <div class="item-wrapper d-md-flex flex-column">
                                        <div class="media">
                                            <picture>
                                                <source data-srcset="{{ asset($value->hinh_anh) }}"
                                                    srcset="{{ asset($value->hinh_anh) }}" />
                                                <img class="lazy" data-src="images/hotels.jpg" src="images/hotels.jpg"
                                                    alt="media" />
                                            </picture>
                                            <span class="media_label media_label--pricing">
                                                <span
                                                    class="price h4">{{ number_format($value->gia_mac_dinh, 0, ',', '.') }}đ</span>
                                                / 1 đêm
                                            </span>
                                        </div>
                                        <div class="main d-md-flex flex-column justify-content-between flex-grow-1">
                                            <a class="main_title h4" href="room.html" data-shave="true">Loại phòng:
                                                <strong>{{ $value->ma_phong }}</strong> </a>
                                            <div class="main_amenities">
                                                <span class="main_amenities-item d-inline-flex align-items-center">
                                                    <i class="icon-user icon"></i>
                                                    Số chỗ: <strong style="margin-left:4px">{{ $value->so_khach }}
                                                        người</strong>
                                                </span>
                                                <br>
                                                <span class="main_amenities-item d-inline-flex align-items-center">
                                                    <i class="icon-bunk_bed icon"></i>
                                                    View: <strong style="margin-left:4px">{{ $value->view }}</strong>
                                                </span>
                                            </div>
                                            <a class="link link--arrow d-inline-flex align-items-center"
                                                href="{{ route('detail-room', $value->id) }}">
                                                Xem chi tiết
                                                <i class="icon-arrow_right icon"></i>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            @endif
                        @endforeach

                    </ul>
                </div>
            </section>
            <!-- rooms section end -->
            <!-- single room content start -->
        </main>
    @endsection
    @section('js')
        <script src="{{ asset('client/js/room.min.js') }}"></script>

        <script>
            $(document).ready(function() {
                var day_begin = localStorage.getItem("day_begin");
                var day_end = localStorage.getItem("day_end");
                var nguoi_lon = localStorage.getItem("nguoi_lon");
                var tre_em = localStorage.getItem("tre_em");
                console.log(day_begin, day_end, nguoi_lon, tre_em);
                var so_luong = 1;

                if(nguoi_lon > 2) {
                    if(nguoi_lon % 2 == 0) {
                        so_luong = Math.floor((nguoi_lon / 2));
                    } else {
                        so_luong = Math.floor((nguoi_lon / 2) + 1);
                    }
                }

                if(tre_em > nguoi_lon) {
                    var so_tre_em = tre_em - nguoi_lon;
                    if(so_tre_em % 2 == 0) {
                        so_luong = so_luong + Math.floor((so_tre_em / 2));
                    } else {
                        so_luong = so_luong + Math.floor((so_tre_em / 2) + 1);
                    }
                }

                $("#checkIn").val(day_begin)
                $("#checkOut").val(day_end)
                $("#so_luong").val(so_luong)

                $('.js-submit-order').click(function() {
                    let $form = $(this).closest('form').serialize();
                    let $url = $(this).closest('form').attr('action');
                    $.ajax({
                        url: $url,
                        type: 'POST',
                        data: $form,
                        success: function(data) {
                            if (data.status == 1) {
                                Swal.fire({
                                    icon: 'success',
                                    title: data.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                                setTimeout(function() {
                                    window.location.reload();
                                }, 2000);
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: data.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }
                        }
                    })
                });
            });
        </script>
    @endsection
