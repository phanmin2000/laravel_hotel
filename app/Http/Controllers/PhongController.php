<?php

namespace App\Http\Controllers;

use App\Http\Requests\CheckIdPhongRequest;
use App\Http\Requests\CreatePhongRequest;
use App\Http\Requests\UpdatePhongRequest;
use App\Models\ChiTietPhong;
use App\Models\ChiTietPhongSuDung;
use App\Models\KhuVuc;
use App\Models\Phong;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PhongController extends Controller
{
    public function index()
    {
        $khuVuc = KhuVuc::all();

        return view('admin.page.phong.index', compact('khuVuc'));
    }

    public function getData()
    {
        $data = Phong::join('khu_vucs', 'phongs.khu_vuc_id', 'khu_vucs.id')
                     ->select('phongs.*', 'khu_vucs.ten_khu')
                     ->get(); // Trả về array

        return response()->json([
            'data'    => $data,
        ]);
    }

    public function viewListRoom(Request $request)
    {
        $dataRequest = $request->all();
        $quality = 0;
        if($dataRequest != [] && $dataRequest['start_date'] != 'Chọn ngày' ) {
            $ngay_bd = Carbon::createFromFormat('m.d.Y', $dataRequest['start_date']);
            $ngay_kt = Carbon::createFromFormat('m.d.Y', $dataRequest['end_date']);

            $format_ngay_bd = $ngay_bd->format('Y-m-d');
            $format_ngay_kt = $ngay_kt->format('Y-m-d');
            $id_phong_su_dung = ChiTietPhongSuDung::query()->whereBetween('ngay_su_dung', [$format_ngay_bd, $format_ngay_kt])
                                                            ->pluck('id_phong');
            $id_phong_su_dung = array_unique($id_phong_su_dung->toArray());
            $data = ChiTietPhong::whereNotIn('id', $id_phong_su_dung)->pluck('id_phong');
            $data = array_unique($data->toArray());
            $data = Phong::where('tinh_trang', 1)
                ->whereIn('id', $data);
            if($dataRequest['nguoilon'] != null){
                $quality += $dataRequest['nguoilon'];
            }

            if($dataRequest['treem'] != null){
                $quality += ($dataRequest['treem'] * 0.5);
            }
            if($quality != 0){
                $data = $data->where('so_khach', '>=', round($quality));
            }
            $data = $data->paginate();
            foreach ($data as $key => $item) {
                $item->view = KhuVuc::where('id', $item->khu_vuc_id)->first()->mo_ta;
                $item->countRoom = ChiTietPhong::query()->where('id_phong', $item->id)->count();
            }
        }else{
            $data = Phong::where('tinh_trang', 1)->paginate();
            foreach ($data as $key => $item) {
                $item->view = KhuVuc::where('id', $item->khu_vuc_id)->first()->mo_ta;
                $item->countRoom = ChiTietPhong::query()->where('id_phong', $item->id)->count();
            }

        }

            return view('client.page.list_room', compact('data'));


    }

    public function store(CreatePhongRequest $request)
    {
        $phong = Phong::create([
            'ma_phong'      =>  $request->ma_phong,
            'gia_mac_dinh'  =>  $request->gia_mac_dinh,
            'mo_ta_phong'   =>  $request->mo_ta_phong,
            'tinh_trang'    =>  $request->tinh_trang,
            'hinh_anh'      =>  $request->hinh_anh,
            'khu_vuc_id'    =>  $request->khu_vuc_id,
            'so_khach'      =>  $request->so_khach,
        ]);

        broadcast(new \App\Events\event_phong(1));

        return response()->json([
            'status'    => true,
        ]);
    }

    public function destroy(CheckIdPhongRequest $request)
    {
        // Phong:where('id', $request->id)->first();
        Phong::find($request->id)->delete();

        return response()->json([
            'status'    => true,
        ]);
    }

    public function edit(CheckIdPhongRequest $request)
    {
        $data = Phong::find($request->id);

        return response()->json([
            'data'    => $data,
        ]);
    }

    public function update(UpdatePhongRequest $request)
    {
        $phong = Phong::find($request->id);
        $phong->ma_phong     = $request->ma_phong;
        $phong->gia_mac_dinh = $request->gia_mac_dinh;
        $phong->mo_ta_phong  = $request->mo_ta_phong;
        $phong->tinh_trang   = $request->tinh_trang;
        $phong->hinh_anh     = $request->hinh_anh;
        $phong->khu_vuc_id   = $request->khu_vuc_id;
        $phong->so_khach     = $request->so_khach;

        $phong->save();

        return response()->json(['status' => true]);
    }

    public function viewDetailRoom($id)
    {
        $user           = Auth::guard('customer')->user();
        $data           = Phong::where('id', $id)->first();
        $data->view     = KhuVuc::where('id', $data->khu_vuc_id)->first()->mo_ta;
        $chitietphong   = ChiTietPhong::where('id_phong', $id)->get();
        $list           = Phong::where('tinh_trang', 1)->where('id', '!=' , $id)->take(3)->get();

        if($data) {
            return view('client.page.detail_room', [
                'data' => $data,
                'chitietphong' => $chitietphong,
                'list' => $list,
                'user' => $user
            ]);
        } else {
            toastr()->error('Liên kết không tồn tại!');
            return redirect('/');
        }
    }

    public function changeStatus($id)
    {
        $phong = Phong::find($id);

        if ($phong) {
            $phong->tinh_trang = !$phong->tinh_trang;
            $phong->save();

            return response()->json([
                'status'    => true,
            ]);
        }
    }
}
