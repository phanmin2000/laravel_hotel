<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerBookingRequest;
use App\Jobs\sendMailJob;
use App\Models\ChiTietPhong;
use App\Models\ChiTietPhongSuDung;
use App\Models\HoaDon;
use App\Models\Phong;
use App\Models\Transaction;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class BookingController extends Controller
{
    public function processBooking(Request $request)
    {
//        $nguoi_login    =   auth()->user();
//        if(!$nguoi_login){
//            return response()->json([
//                'status' => -1,
//                'message' => 'Yêu cầu đăng nhập để đặt phòng'
//            ]);
//        }d
        $data = $request->all();
        $phong = Phong::where('id', $data['room_id'])->first();
        $chitietphong = ChiTietPhong::where('id_phong', $data['room_id'])->where('is_open',1)->get();
        if($chitietphong->count() == 0) {
            return response()->json([
                'status' => -1,
                'message' => 'Loại phòng này đã hết phòng trống vào thời gian bạn chọn'
            ]);
        }else{
            $this->taoHoaDon($data);
            return response()->json([
                'status' => 1,
                'message' => 'Kiểm tra email để xác nhận yêu cầu đặt phòng !',
            ]);
        }



    }

    public function taoHoaDon($data)
    {
//        DB::beginTransaction();
//        try {
            $ngay_bd = Carbon::createFromFormat('m.d.Y', $data['start_date']);
            $ngay_kt = Carbon::createFromFormat('m.d.Y', $data['end_date']);
            $so_ngay    = $ngay_kt->diffInDays($ngay_bd);

            $phong      = Phong::where('id', $data['room_id'])->first();
            $tong_tien  = $phong->gia_mac_dinh * $so_ngay * $data['quantity'];

            $hoaDon     =  HoaDon::create([
                'ho_va_ten'         =>  $data['name'],
                'email'             =>  $data['email'],
                'ngay_bat_dau'      =>  $ngay_bd->format('Y-m-d'),
                'ngay_ket_thuc'     =>  $ngay_kt->format('Y-m-d'),
                'so_phong_dat'      =>  $data['quantity'],
                'loai_phong_dat'    =>  $phong->id,
                'tong_tien'         =>  $tong_tien,
            ]);
            $so_hoa_don = $hoaDon->id * 1 + 10000;

            $data['ho_va_ten']                  =   $data['name'];
            $data['tu_ngay']                    =   $data['start_date'];
            $data['den_ngay']                   =   $data['end_date'];
            $data['so_phong_dat']               =   $data['quantity'] .' phòng';
            $data['loai_phong_dat']             =   $phong->ma_phong;
            $data['tong_tien_thanh_toan']       =   number_format($tong_tien, 0) . ' đ';
            $data['tien_dat_coc']               =   number_format($tong_tien * 0.3, 0) . ' đ';
            $data['noi_dung_chuyen_khoan']      =   $so_hoa_don;


            $vnp_Url = "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
            $vnp_Returnurl = route('payment.vnpay.return',['code_return' => $so_hoa_don]);


            $vnp_TmnCode = "2TNEXMQ8";
            $vnp_HashSecret = "NTELFBCCDPWBIXLZQRTUCNLOYNVFNANA";

            //
            $vnp_TxnRef = 'order_'.$so_hoa_don;  // Mã đơn hàng;
            $vnp_OrderInfo = 'Thanh toán cọc tiền khách sạn'; // Noi dung thanh toan;
            $vnp_OrderType = "Thanh toán cọc tiền khách sạn";
            $vnp_Amount = $tong_tien * 0.3 * 100;
            $vnp_Locale = "VN";
            $vnp_BankCode = "NCB";
            $vnp_IpAddr = $_SERVER['REMOTE_ADDR'];

            $inputData = array(
                "vnp_Version" => "2.1.0",
                "vnp_TmnCode" => $vnp_TmnCode,
                "vnp_Amount" => $vnp_Amount,
                "vnp_Command" => "pay",
                "vnp_CreateDate" => date('YmdHis'),
                "vnp_CurrCode" => "VND",
                "vnp_IpAddr" => $vnp_IpAddr,
                "vnp_Locale" => $vnp_Locale,
                "vnp_OrderInfo" => $vnp_OrderInfo,
                "vnp_OrderType" => $vnp_OrderType,
                "vnp_ReturnUrl" => $vnp_Returnurl,
                "vnp_TxnRef" => $vnp_TxnRef,
            );

            if (isset($vnp_BankCode) && $vnp_BankCode != "") {
                $inputData['vnp_BankCode'] = $vnp_BankCode;
            }

            ksort($inputData);
            $query = "";
            $i = 0;
            $hashdata = "";
            foreach ($inputData as $key => $value) {
                if ($i == 1) {
                    $hashdata .= '&' . urlencode($key) . "=" . urlencode($value);
                } else {
                    $hashdata .= urlencode($key) . "=" . urlencode($value);
                    $i = 1;
                }
                $query .= urlencode($key) . "=" . urlencode($value) . '&';
            }

            $vnp_Url = $vnp_Url . "?" . $query;
            if (isset($vnp_HashSecret)) {
                $vnpSecureHash =   hash_hmac('sha512', $hashdata, $vnp_HashSecret);//
                $vnp_Url .= 'vnp_SecureHash=' . $vnpSecureHash;
            }
            $data['vnp_Url'] = $vnp_Url;
            sendMailJob::dispatch($data['email'], 'Xác Nhận Đặt Phòng', $data, 'mail.order');

            Log::info("Yêu cầu của bạn đã được gửi, vui lòng check mail để xác nhận đặt phòng");

//            DB::commit();
//        } catch(Exception $e) {
//            Log::error("Có lỗi " . $e);
//            DB::rollBack();
//        }
    }
    public function vnpayReturn(Request $request){
        $code = $request->code_return;
        $code = $code - 10000;
        $hoadon = HoaDon::where('id',$code)->first();
        if($hoadon->is_don_hang == 1){
            return redirect()->route('home');
        }
        $pay = (int) ($request->vnp_Amount / 100);
        $hoadon->tien_coc = $pay;
        $hoadon->thanh_toan = 1;
        $hoadon->is_don_hang = 1;
        $hoadon->xep_phong = 1;
        $hoadon->save();

        $soluong = $hoadon->so_phong_dat;
        $phong = Phong::where('id',$hoadon->loai_phong_dat)->first();
        $rooms = [];
        for ($i =1;$i<=$soluong; $i ++){
            $listActice = ChiTietPhong::query()->where('id_phong',$phong->id)->where('is_open',1)->first();
            $listActice->is_open = 0;
            $listActice->save();

            $ngay_bd = Carbon::createFromFormat('Y-m-d', $hoadon->ngay_bat_dau);
            $ngay_kt = Carbon::createFromFormat('Y-m-d', $hoadon->ngay_ket_thuc);
            foreach ($ngay_bd->daysUntil($ngay_kt) as $date) {
                $date = $date->format('Y-m-d');
                ChiTietPhongSuDung::insert([
                    'id_hoa_don' => $hoadon->id,
                    'id_phong' => $listActice->id,
                    'ngay_su_dung' => $date,
                    'ho_ten_khach_hang' => $hoadon->ho_va_ten,
                    'so_dien_thoai_khach_hang' => $hoadon->email,
                ]);
            }
            $rooms[] = $listActice;
        }
        return view('client.page.success',[
            'rooms' => $rooms
        ]);
    }

    public function confirmCoc(Request $request){
        $hoadon = HoaDon::where('id',$request->id)->first();
        $pay = (int) ($request->tong_tien *0.3);
        $hoadon->tien_coc = $pay;
        $hoadon->thanh_toan = 1;
        $hoadon->is_don_hang = 1;
        $hoadon->xep_phong = 1;
        $hoadon->save();

        $soluong = $hoadon->so_phong_dat;
        $phong = Phong::where('id',$hoadon->loai_phong_dat)->first();
        for ($i =1;$i<=$soluong; $i ++){
            $listActice = ChiTietPhong::query()->where('id_phong',$phong->id)->where('is_open',1)->first();
            $listActice->is_open = 0;
            $listActice->save();

            $ngay_bd = Carbon::createFromFormat('Y-m-d', $hoadon->ngay_bat_dau);
            $ngay_kt = Carbon::createFromFormat('Y-m-d', $hoadon->ngay_ket_thuc);
            foreach ($ngay_bd->daysUntil($ngay_kt) as $date) {
                $date = $date->format('Y-m-d');
                ChiTietPhongSuDung::insert([
                    'id_hoa_don' => $hoadon->id,
                    'id_phong' => $listActice->id,
                    'ngay_su_dung' => $date,
                    'ho_ten_khach_hang' => $hoadon->ho_va_ten,
                    'so_dien_thoai_khach_hang' => $hoadon->email,
                ]);
            }
        }
        return response()->json([
            'status' => 1,
        ]);
    }
    // public function transactionBooking()
    // {
    //     $now = Carbon::now()->format('d/m/Y');
    //     $client = new Client([
    //         'headers' => [ 'Content-Type' => 'application/json' ]
    //     ]);
    //     echo 'đang chạy code..... <br>';
    //     $response = $client->post('http://103.137.185.207:9899/api/vcb/transactions',
    //                 [
    //                     'body' => json_encode(
    //                         [
    //                             'begin'           => $now,
    //                             'end'             => $now,
    //                             'username'        => '0982834671',
    //                             'password'        => 'Anhvu.372002',
    //                         ]
    //                 )]
    //             );

    //     $result = json_decode($response->getBody()->getContents(), true);

    //     if($result["success"] == true) {
    //         $list = $result["transactions"];
    //         foreach($list as $key => $value) {
    //             $tran = Transaction::where('reference', $value['Reference'])->first();
    //             if(!$tran && $value['CD'] == '+') {
    //                 $str = $value['Description'];
    //                 $hd  = strpos($str, 'HD') ? Str::substr($str, 2 + strpos($str, 'HD')) : '';
    //                 Transaction::create([
    //                     'reference'         => $value['Reference'],
    //                     'amount'            => $value['Amount'],
    //                     'description'       => $value['Description'],
    //                     'content'           => $hd,
    //                 ]);

    //                 if(strlen($hd) > 4) {
    //                     // hd = 10009
    //                     $id = $hd - 10000;
    //                     $hoaDon = HoaDon::where('id', $id)->first();
    //                     if($hoaDon) {
    //                         $tien_can_dat_coc = $hoaDon->tong_tien * 0.3;
    //                         if($tien_can_dat_coc <= $value['Amount']) {
    //                             $hoaDon->tien_coc   = $value['Amount'];
    //                             $hoaDon->thanh_toan = 1;
    //                             $hoaDon->save();
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //     }
    // }
}
